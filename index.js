const express = require('express')
const mongoose = require('mongoose')
require('dotenv').config() // Initialize ENV file
const app = express()
const port = 3000

//======================== Mongoose connection

mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.jvcc8cx.mongodb.net/b271-todo-db?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection 

db.on("error", () => console.log("Connection error"))
db.once("open", ()=> console.log("Connected to MongoDB!"))

//======================== Schema
const user_schema = new mongoose.Schema({
	username: String,
	password: String
})

//======================== Model
const User = mongoose.model("User", user_schema)

//======================== Middleware registration
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//======================== Register user
app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username}).then((result, error) => {
		if(result !== null && result.username == request.body.username){
			return response.send("Duplicate user has been found!")
		}
		else { 

			let new_user = new User({
				username: request.body.username,
				password: request.body.password
			})

			new_user.save().then((registered_user, error) => {
				if(error){
					return console.log(error)
				} else {
					return response.status(201).send("New user registered")
				}
			})
		}
	})
})

app.get('/signup', (request, response) => {
	User.find({}).then((result, error) => {
		if(error){
			return console.log(error)
		}
		return response.status(200).send(result)
	})
})


app.listen(port, () => console.log(`The server is running at localhost: ${port}`))